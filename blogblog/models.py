from __future__ import unicode_literals
from django.db import models

# Create your models here.
class Posts(models.Model):
    TITLE_MAX_LENGTH = 100
    AUTHOR_NAME_MAX_LENGTH = 100
    title = models.CharField(max_length=TITLE_MAX_LENGTH)
    author_name = models.CharField(max_length=AUTHOR_NAME_MAX_LENGTH)
    test = models.TextField
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{author} : {title}".format(author=self.author_name,title=self.title)



